class STYLE:
    background='gray90'
    foreground='black'
    entry_background='white'
    list_background='white'
    list_select_background='#00008b'
    list_select_foreground='grey'
    tooltip_background="yellow"

    standard = ("Helvetica",12)
    standard_italique = ("Helvetica",12,'italic')
    standard_gras = ("Helvetica",12,'bold')
    standard_gras_souligne = ("Helvetica",12,'bold','underline')

    canvas = ('Helvetica',10)
    canvas_italique = ('Helvetica',10,'italic')
    canvas_gras = ("Helvetica",10,'bold')
    canvas_gras_italique = ("Helvetica",12,'bold','italic')

    standard12 = ("Helvetica",14)
    standard12_gras = ("Helvetica",14,'bold')
    standard12_gras_italique = ( "Helvetica",14,'bold','italic')

    standardcourier10 = ("Courier",14)
    statusfont = ("Helvetica",16)

style=STYLE()

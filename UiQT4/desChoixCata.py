# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/local01/salome/V6_4_0rc2/tools/src/Eficasv1-V6_4_0rc2-py266-qt463p1-sip4112-pyqt481-cm285/UiQT4/desChoixCata.ui'
#
# Created: Mon Dec  5 18:57:13 2011
#      by: PyQt4 UI code generator 4.8.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_DChoixCata(object):
    def setupUi(self, DChoixCata):
        DChoixCata.setObjectName(_fromUtf8("DChoixCata"))
        DChoixCata.resize(465, 271)
        DChoixCata.setSizeGripEnabled(True)
        self.gridLayout = QtGui.QGridLayout(DChoixCata)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.TLNb = QtGui.QLabel(DChoixCata)
        self.TLNb.setMinimumSize(QtCore.QSize(0, 0))
        self.TLNb.setWordWrap(False)
        self.TLNb.setObjectName(_fromUtf8("TLNb"))
        self.horizontalLayout_3.addWidget(self.TLNb)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.gridLayout.addLayout(self.horizontalLayout_3, 0, 0, 1, 2)
        spacerItem1 = QtGui.QSpacerItem(20, 45, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem1, 1, 1, 1, 1)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.textLabel1_2 = QtGui.QLabel(DChoixCata)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.textLabel1_2.sizePolicy().hasHeightForWidth())
        self.textLabel1_2.setSizePolicy(sizePolicy)
        self.textLabel1_2.setMinimumSize(QtCore.QSize(0, 0))
        self.textLabel1_2.setScaledContents(False)
        self.textLabel1_2.setWordWrap(False)
        self.textLabel1_2.setObjectName(_fromUtf8("textLabel1_2"))
        self.horizontalLayout_2.addWidget(self.textLabel1_2)
        spacerItem2 = QtGui.QSpacerItem(38, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem2)
        self.CBChoixCata = QtGui.QComboBox(DChoixCata)
        self.CBChoixCata.setEnabled(True)
        self.CBChoixCata.setMinimumSize(QtCore.QSize(125, 41))
        self.CBChoixCata.setMaximumSize(QtCore.QSize(150, 16777215))
        self.CBChoixCata.setObjectName(_fromUtf8("CBChoixCata"))
        self.horizontalLayout_2.addWidget(self.CBChoixCata)
        self.gridLayout.addLayout(self.horizontalLayout_2, 2, 0, 1, 2)
        spacerItem3 = QtGui.QSpacerItem(20, 45, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem3, 3, 0, 1, 1)
        self.frame3 = QtGui.QFrame(DChoixCata)
        self.frame3.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame3.setFrameShadow(QtGui.QFrame.Raised)
        self.frame3.setObjectName(_fromUtf8("frame3"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.frame3)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.buttonOk = QtGui.QPushButton(self.frame3)
        self.buttonOk.setMinimumSize(QtCore.QSize(81, 41))
        self.buttonOk.setShortcut(_fromUtf8(""))
        self.buttonOk.setAutoDefault(True)
        self.buttonOk.setDefault(True)
        self.buttonOk.setObjectName(_fromUtf8("buttonOk"))
        self.horizontalLayout.addWidget(self.buttonOk)
        spacerItem4 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem4)
        self.buttonCancel = QtGui.QPushButton(self.frame3)
        self.buttonCancel.setMinimumSize(QtCore.QSize(81, 41))
        self.buttonCancel.setShortcut(_fromUtf8(""))
        self.buttonCancel.setAutoDefault(True)
        self.buttonCancel.setObjectName(_fromUtf8("buttonCancel"))
        self.horizontalLayout.addWidget(self.buttonCancel)
        self.gridLayout.addWidget(self.frame3, 4, 0, 1, 2)

        self.retranslateUi(DChoixCata)
        QtCore.QMetaObject.connectSlotsByName(DChoixCata)

    def retranslateUi(self, DChoixCata):
        DChoixCata.setWindowTitle(QtGui.QApplication.translate("DChoixCata", "Choix d\'une version du code Aster", None, QtGui.QApplication.UnicodeUTF8))
        self.TLNb.setText(QtGui.QApplication.translate("DChoixCata", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Sans Serif\'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:11pt;\">2 versions sont disponibles</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.textLabel1_2.setText(QtGui.QApplication.translate("DChoixCata", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Sans Serif\'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<table style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:large;\">Veuillez choisir celle avec laquelle</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:large;\"> vous souhaitez travailler</span></p></td></tr></table></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.buttonOk.setText(QtGui.QApplication.translate("DChoixCata", "&OK", None, QtGui.QApplication.UnicodeUTF8))
        self.buttonCancel.setText(QtGui.QApplication.translate("DChoixCata", "&Cancel", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    DChoixCata = QtGui.QDialog()
    ui = Ui_DChoixCata()
    ui.setupUi(DChoixCata)
    DChoixCata.show()
    sys.exit(app.exec_())


# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/local01/salome/V6_4_0rc2/tools/src/Eficasv1-V6_4_0rc2-py266-qt463p1-sip4112-pyqt481-cm285/UiQT4/desPlusieursInto.ui'
#
# Created: Mon Dec  5 18:57:13 2011
#      by: PyQt4 UI code generator 4.8.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_DPlusInto(object):
    def setupUi(self, DPlusInto):
        DPlusInto.setObjectName(_fromUtf8("DPlusInto"))
        DPlusInto.resize(482, 480)
        DPlusInto.setMinimumSize(QtCore.QSize(350, 0))
        self.gridlayout = QtGui.QGridLayout(DPlusInto)
        self.gridlayout.setObjectName(_fromUtf8("gridlayout"))
        self.tabuniqueinto = QtGui.QTabWidget(DPlusInto)
        self.tabuniqueinto.setObjectName(_fromUtf8("tabuniqueinto"))
        self.Widget8 = QtGui.QWidget()
        self.Widget8.setObjectName(_fromUtf8("Widget8"))
        self.gridLayout = QtGui.QGridLayout(self.Widget8)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.textLabel1 = QtGui.QLabel(self.Widget8)
        self.textLabel1.setWordWrap(False)
        self.textLabel1.setObjectName(_fromUtf8("textLabel1"))
        self.gridLayout.addWidget(self.textLabel1, 0, 0, 1, 1)
        self.textLabel1_2 = QtGui.QLabel(self.Widget8)
        self.textLabel1_2.setWordWrap(False)
        self.textLabel1_2.setObjectName(_fromUtf8("textLabel1_2"))
        self.gridLayout.addWidget(self.textLabel1_2, 0, 1, 1, 1)
        self.hboxlayout = QtGui.QHBoxLayout()
        self.hboxlayout.setObjectName(_fromUtf8("hboxlayout"))
        self.LBValeurs = QtGui.QListWidget(self.Widget8)
        self.LBValeurs.setObjectName(_fromUtf8("LBValeurs"))
        self.hboxlayout.addWidget(self.LBValeurs)
        self.vboxlayout = QtGui.QVBoxLayout()
        self.vboxlayout.setObjectName(_fromUtf8("vboxlayout"))
        spacerItem = QtGui.QSpacerItem(21, 44, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.vboxlayout.addItem(spacerItem)
        self.vboxlayout1 = QtGui.QVBoxLayout()
        self.vboxlayout1.setObjectName(_fromUtf8("vboxlayout1"))
        self.BAjout1Val = QtGui.QToolButton(self.Widget8)
        self.BAjout1Val.setMinimumSize(QtCore.QSize(40, 31))
        self.BAjout1Val.setMaximumSize(QtCore.QSize(40, 31))
        self.BAjout1Val.setText(_fromUtf8(""))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("../Editeur/icons/arrow_left.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.BAjout1Val.setIcon(icon)
        self.BAjout1Val.setObjectName(_fromUtf8("BAjout1Val"))
        self.vboxlayout1.addWidget(self.BAjout1Val)
        self.BSup1Val = QtGui.QToolButton(self.Widget8)
        self.BSup1Val.setMinimumSize(QtCore.QSize(40, 31))
        self.BSup1Val.setMaximumSize(QtCore.QSize(40, 31))
        self.BSup1Val.setText(_fromUtf8(""))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8("../Editeur/icons/arrow_right.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.BSup1Val.setIcon(icon1)
        self.BSup1Val.setObjectName(_fromUtf8("BSup1Val"))
        self.vboxlayout1.addWidget(self.BSup1Val)
        spacerItem1 = QtGui.QSpacerItem(21, 176, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.vboxlayout1.addItem(spacerItem1)
        self.vboxlayout.addLayout(self.vboxlayout1)
        self.hboxlayout.addLayout(self.vboxlayout)
        self.listBoxVal = QtGui.QListWidget(self.Widget8)
        self.listBoxVal.setObjectName(_fromUtf8("listBoxVal"))
        self.hboxlayout.addWidget(self.listBoxVal)
        self.gridLayout.addLayout(self.hboxlayout, 1, 0, 1, 2)
        self.Commentaire = QtGui.QLabel(self.Widget8)
        self.Commentaire.setMinimumSize(QtCore.QSize(0, 20))
        self.Commentaire.setText(_fromUtf8(""))
        self.Commentaire.setWordWrap(False)
        self.Commentaire.setObjectName(_fromUtf8("Commentaire"))
        self.gridLayout.addWidget(self.Commentaire, 2, 0, 1, 2)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem2 = QtGui.QSpacerItem(148, 27, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem2)
        self.bOk = QtGui.QPushButton(self.Widget8)
        self.bOk.setMinimumSize(QtCore.QSize(130, 30))
        self.bOk.setAutoDefault(True)
        self.bOk.setDefault(True)
        self.bOk.setObjectName(_fromUtf8("bOk"))
        self.horizontalLayout.addWidget(self.bOk)
        spacerItem3 = QtGui.QSpacerItem(58, 27, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem3)
        self.BAlpha = QtGui.QPushButton(self.Widget8)
        self.BAlpha.setObjectName(_fromUtf8("BAlpha"))
        self.horizontalLayout.addWidget(self.BAlpha)
        self.gridLayout.addLayout(self.horizontalLayout, 3, 0, 1, 2)
        self.tabuniqueinto.addTab(self.Widget8, _fromUtf8(""))
        self.gridlayout.addWidget(self.tabuniqueinto, 0, 0, 1, 1)

        self.retranslateUi(DPlusInto)
        QtCore.QMetaObject.connectSlotsByName(DPlusInto)
        DPlusInto.setTabOrder(self.listBoxVal, self.tabuniqueinto)
        DPlusInto.setTabOrder(self.tabuniqueinto, self.bOk)
        DPlusInto.setTabOrder(self.bOk, self.LBValeurs)

    def retranslateUi(self, DPlusInto):
        DPlusInto.setWindowTitle(QtGui.QApplication.translate("DPlusInto", "DUnIn", None, QtGui.QApplication.UnicodeUTF8))
        self.textLabel1.setText(QtGui.QApplication.translate("DPlusInto", "<u><font size=\"+1\">Valeur(s) actuelle(s)</font></u>", None, QtGui.QApplication.UnicodeUTF8))
        self.textLabel1_2.setText(QtGui.QApplication.translate("DPlusInto", "<u><font size=\"+1\">Valeur(s) possibles(s)</font></u>", None, QtGui.QApplication.UnicodeUTF8))
        self.BAjout1Val.setToolTip(QtGui.QApplication.translate("DPlusInto", "enleve l occurence selectionnee", None, QtGui.QApplication.UnicodeUTF8))
        self.BSup1Val.setToolTip(QtGui.QApplication.translate("DPlusInto", "ajoute la valeur saisie sous l occurence selectionnée (en fin de liste si il n y a pas de selection)", None, QtGui.QApplication.UnicodeUTF8))
        self.bOk.setToolTip(QtGui.QApplication.translate("DPlusInto", "validation de la saisie", None, QtGui.QApplication.UnicodeUTF8))
        self.bOk.setText(QtGui.QApplication.translate("DPlusInto", "&Valider", None, QtGui.QApplication.UnicodeUTF8))
        self.bOk.setShortcut(QtGui.QApplication.translate("DPlusInto", "Shift+A, Alt+A, Alt+A, Alt+A", None, QtGui.QApplication.UnicodeUTF8))
        self.BAlpha.setText(QtGui.QApplication.translate("DPlusInto", "Tri Alpha", None, QtGui.QApplication.UnicodeUTF8))
        self.tabuniqueinto.setTabText(self.tabuniqueinto.indexOf(self.Widget8), QtGui.QApplication.translate("DPlusInto", "Saisir Valeur", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    DPlusInto = QtGui.QWidget()
    ui = Ui_DPlusInto()
    ui.setupUi(DPlusInto)
    DPlusInto.show()
    sys.exit(app.exec_())


# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/local01/salome/V6_4_0rc2/tools/src/Eficasv1-V6_4_0rc2-py266-qt463p1-sip4112-pyqt481-cm285/UiQT4/OptionsPdf.ui'
#
# Created: Mon Dec  5 18:57:13 2011
#      by: PyQt4 UI code generator 4.8.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_desPdf(object):
    def setupUi(self, desPdf):
        desPdf.setObjectName(_fromUtf8("desPdf"))
        desPdf.resize(538, 142)
        self.gridLayout = QtGui.QGridLayout(desPdf)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.textLabel1_2 = QtGui.QLabel(desPdf)
        self.textLabel1_2.setWordWrap(False)
        self.textLabel1_2.setObjectName(_fromUtf8("textLabel1_2"))
        self.horizontalLayout_2.addWidget(self.textLabel1_2)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.gridLayout.addLayout(self.horizontalLayout_2, 0, 0, 1, 1)
        self.LERepPdf = QtGui.QLineEdit(desPdf)
        self.LERepPdf.setMinimumSize(QtCore.QSize(0, 30))
        self.LERepPdf.setObjectName(_fromUtf8("LERepPdf"))
        self.gridLayout.addWidget(self.LERepPdf, 1, 0, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.Bok = QtGui.QPushButton(desPdf)
        self.Bok.setObjectName(_fromUtf8("Bok"))
        self.horizontalLayout.addWidget(self.Bok)
        self.BCancel = QtGui.QPushButton(desPdf)
        self.BCancel.setObjectName(_fromUtf8("BCancel"))
        self.horizontalLayout.addWidget(self.BCancel)
        self.gridLayout.addLayout(self.horizontalLayout, 2, 0, 1, 1)

        self.retranslateUi(desPdf)
        QtCore.QMetaObject.connectSlotsByName(desPdf)
        desPdf.setTabOrder(self.LERepPdf, self.Bok)
        desPdf.setTabOrder(self.Bok, self.BCancel)

    def retranslateUi(self, desPdf):
        desPdf.setWindowTitle(QtGui.QApplication.translate("desPdf", "Lire les Pdf", None, QtGui.QApplication.UnicodeUTF8))
        self.textLabel1_2.setText(QtGui.QApplication.translate("desPdf", "Lecteur Pdf :", None, QtGui.QApplication.UnicodeUTF8))
        self.LERepPdf.setText(QtGui.QApplication.translate("desPdf", "acroread", None, QtGui.QApplication.UnicodeUTF8))
        self.Bok.setText(QtGui.QApplication.translate("desPdf", "Ok", None, QtGui.QApplication.UnicodeUTF8))
        self.BCancel.setText(QtGui.QApplication.translate("desPdf", "Cancel", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    desPdf = QtGui.QDialog()
    ui = Ui_desPdf()
    ui.setupUi(desPdf)
    desPdf.show()
    sys.exit(app.exec_())


# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/local01/salome/V6_4_0rc2/tools/src/Eficasv1-V6_4_0rc2-py266-qt463p1-sip4112-pyqt481-cm285/UiQT4/desVisu.ui'
#
# Created: Mon Dec  5 18:57:13 2011
#      by: PyQt4 UI code generator 4.8.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_DVisu(object):
    def setupUi(self, DVisu):
        DVisu.setObjectName(_fromUtf8("DVisu"))
        DVisu.resize(501, 394)
        self.gridlayout = QtGui.QGridLayout(DVisu)
        self.gridlayout.setObjectName(_fromUtf8("gridlayout"))
        self.TB = QtGui.QTextBrowser(DVisu)
        self.TB.setObjectName(_fromUtf8("TB"))
        self.gridlayout.addWidget(self.TB, 0, 0, 1, 1)

        self.retranslateUi(DVisu)

    def retranslateUi(self, DVisu):
        DVisu.setWindowTitle(QtGui.QApplication.translate("DVisu", "Visualisation Include Materiau", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    DVisu = QtGui.QWidget()
    ui = Ui_DVisu()
    ui.setupUi(DVisu)
    DVisu.show()
    sys.exit(app.exec_())


# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/local01/salome/V6_4_0rc2/tools/src/Eficasv1-V6_4_0rc2-py266-qt463p1-sip4112-pyqt481-cm285/UiQT4/desMatrice.ui'
#
# Created: Mon Dec  5 18:57:13 2011
#      by: PyQt4 UI code generator 4.8.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_desMatrice(object):
    def setupUi(self, desMatrice):
        desMatrice.setObjectName(_fromUtf8("desMatrice"))
        desMatrice.resize(400, 300)
        self.gridLayout_2 = QtGui.QGridLayout(desMatrice)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.TBMatrice = QtGui.QTableWidget(desMatrice)
        self.TBMatrice.setObjectName(_fromUtf8("TBMatrice"))
        self.TBMatrice.setColumnCount(0)
        self.TBMatrice.setRowCount(0)
        self.gridLayout_2.addWidget(self.TBMatrice, 0, 0, 1, 1)
        self.splitter = QtGui.QSplitter(desMatrice)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName(_fromUtf8("splitter"))
        self.layoutWidget = QtGui.QWidget(self.splitter)
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.gridLayout = QtGui.QGridLayout(self.layoutWidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        spacerItem = QtGui.QSpacerItem(140, 24, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 0, 0, 1, 1)
        self.BOk = QtGui.QPushButton(self.layoutWidget)
        self.BOk.setObjectName(_fromUtf8("BOk"))
        self.gridLayout.addWidget(self.BOk, 0, 1, 1, 1)
        spacerItem1 = QtGui.QSpacerItem(138, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 0, 2, 1, 1)
        self.gridLayout_2.addWidget(self.splitter, 1, 0, 1, 1)

        self.retranslateUi(desMatrice)
        QtCore.QMetaObject.connectSlotsByName(desMatrice)

    def retranslateUi(self, desMatrice):
        desMatrice.setWindowTitle(QtGui.QApplication.translate("desMatrice", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.BOk.setText(QtGui.QApplication.translate("desMatrice", "Valider", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    desMatrice = QtGui.QDialog()
    ui = Ui_desMatrice()
    ui.setupUi(desMatrice)
    desMatrice.show()
    sys.exit(app.exec_())


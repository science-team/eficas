# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/local01/salome/V6_4_0rc2/tools/src/Eficasv1-V6_4_0rc2-py266-qt463p1-sip4112-pyqt481-cm285/UiQT4/desMCFact.ui'
#
# Created: Mon Dec  5 18:57:13 2011
#      by: PyQt4 UI code generator 4.8.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_DMCFact(object):
    def setupUi(self, DMCFact):
        DMCFact.setObjectName(_fromUtf8("DMCFact"))
        DMCFact.resize(511, 499)
        DMCFact.setMinimumSize(QtCore.QSize(505, 0))
        self.gridlayout = QtGui.QGridLayout(DMCFact)
        self.gridlayout.setObjectName(_fromUtf8("gridlayout"))
        self.TWChoix = QtGui.QTabWidget(DMCFact)
        self.TWChoix.setObjectName(_fromUtf8("TWChoix"))
        self.MotClef = QtGui.QWidget()
        self.MotClef.setObjectName(_fromUtf8("MotClef"))
        self.gridlayout1 = QtGui.QGridLayout(self.MotClef)
        self.gridlayout1.setObjectName(_fromUtf8("gridlayout1"))
        self.textLabel1 = QtGui.QLabel(self.MotClef)
        self.textLabel1.setMinimumSize(QtCore.QSize(0, 0))
        self.textLabel1.setWordWrap(False)
        self.textLabel1.setObjectName(_fromUtf8("textLabel1"))
        self.gridlayout1.addWidget(self.textLabel1, 0, 0, 1, 1)
        self.LBMCPermis = QtGui.QListWidget(self.MotClef)
        self.LBMCPermis.setMinimumSize(QtCore.QSize(0, 0))
        self.LBMCPermis.setObjectName(_fromUtf8("LBMCPermis"))
        self.gridlayout1.addWidget(self.LBMCPermis, 1, 0, 1, 1)
        self.LBRegles = QtGui.QListWidget(self.MotClef)
        self.LBRegles.setObjectName(_fromUtf8("LBRegles"))
        self.gridlayout1.addWidget(self.LBRegles, 1, 1, 1, 1)
        self.textLabel1_2 = QtGui.QLabel(self.MotClef)
        self.textLabel1_2.setWordWrap(False)
        self.textLabel1_2.setObjectName(_fromUtf8("textLabel1_2"))
        self.gridlayout1.addWidget(self.textLabel1_2, 0, 1, 1, 1)
        self.TWChoix.addTab(self.MotClef, _fromUtf8(""))
        self.gridlayout.addWidget(self.TWChoix, 0, 0, 1, 3)
        self.Commentaire = QtGui.QLabel(DMCFact)
        self.Commentaire.setText(_fromUtf8(""))
        self.Commentaire.setWordWrap(False)
        self.Commentaire.setObjectName(_fromUtf8("Commentaire"))
        self.gridlayout.addWidget(self.Commentaire, 1, 0, 1, 3)
        self.bOk = QtGui.QPushButton(DMCFact)
        self.bOk.setMinimumSize(QtCore.QSize(159, 0))
        self.bOk.setAutoDefault(True)
        self.bOk.setDefault(True)
        self.bOk.setObjectName(_fromUtf8("bOk"))
        self.gridlayout.addWidget(self.bOk, 2, 2, 1, 1)
        self.BAlpha = QtGui.QPushButton(DMCFact)
        self.BAlpha.setObjectName(_fromUtf8("BAlpha"))
        self.gridlayout.addWidget(self.BAlpha, 2, 0, 1, 1)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridlayout.addItem(spacerItem, 2, 1, 1, 1)

        self.retranslateUi(DMCFact)
        QtCore.QMetaObject.connectSlotsByName(DMCFact)

    def retranslateUi(self, DMCFact):
        DMCFact.setWindowTitle(QtGui.QApplication.translate("DMCFact", "DMacro", None, QtGui.QApplication.UnicodeUTF8))
        self.textLabel1.setText(QtGui.QApplication.translate("DMCFact", "<h3><p align=\"center\"><u><b>Mots Clefs Permis</b></u></p></h3>", None, QtGui.QApplication.UnicodeUTF8))
        self.textLabel1_2.setText(QtGui.QApplication.translate("DMCFact", "<h3><p align=\"center\"><u><b>Régles</b></u></p></h3>", None, QtGui.QApplication.UnicodeUTF8))
        self.TWChoix.setTabText(self.TWChoix.indexOf(self.MotClef), QtGui.QApplication.translate("DMCFact", "Ajouter Mot-Clef", None, QtGui.QApplication.UnicodeUTF8))
        self.bOk.setToolTip(QtGui.QApplication.translate("DMCFact", "validation de la saisie", None, QtGui.QApplication.UnicodeUTF8))
        self.bOk.setText(QtGui.QApplication.translate("DMCFact", "&Valider", None, QtGui.QApplication.UnicodeUTF8))
        self.bOk.setShortcut(QtGui.QApplication.translate("DMCFact", "Shift+A, Alt+A, Alt+A, Alt+A", None, QtGui.QApplication.UnicodeUTF8))
        self.BAlpha.setText(QtGui.QApplication.translate("DMCFact", "Tri Alpha", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    DMCFact = QtGui.QWidget()
    ui = Ui_DMCFact()
    ui.setupUi(DMCFact)
    DMCFact.show()
    sys.exit(app.exec_())


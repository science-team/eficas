# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/local01/salome/V6_4_0rc2/tools/src/Eficasv1-V6_4_0rc2-py266-qt463p1-sip4112-pyqt481-cm285/UiQT4/desCommentaire.ui'
#
# Created: Mon Dec  5 18:57:13 2011
#      by: PyQt4 UI code generator 4.8.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_DComment(object):
    def setupUi(self, DComment):
        DComment.setObjectName(_fromUtf8("DComment"))
        DComment.resize(796, 618)
        DComment.setMinimumSize(QtCore.QSize(505, 0))
        self.gridLayout_3 = QtGui.QGridLayout(DComment)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.TWChoix = QtGui.QTabWidget(DComment)
        self.TWChoix.setObjectName(_fromUtf8("TWChoix"))
        self.Valeur_Parametre = QtGui.QWidget()
        self.Valeur_Parametre.setObjectName(_fromUtf8("Valeur_Parametre"))
        self.gridLayout = QtGui.QGridLayout(self.Valeur_Parametre)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.textCommentaire = QtGui.QTextEdit(self.Valeur_Parametre)
        self.textCommentaire.setObjectName(_fromUtf8("textCommentaire"))
        self.gridLayout.addWidget(self.textCommentaire, 0, 0, 1, 1)
        self.TWChoix.addTab(self.Valeur_Parametre, _fromUtf8(""))
        self.Commande = QtGui.QWidget()
        self.Commande.setObjectName(_fromUtf8("Commande"))
        self.gridLayout_4 = QtGui.QGridLayout(self.Commande)
        self.gridLayout_4.setObjectName(_fromUtf8("gridLayout_4"))
        self.gridLayout_2 = QtGui.QGridLayout()
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.textLabel1_4 = QtGui.QLabel(self.Commande)
        self.textLabel1_4.setWordWrap(False)
        self.textLabel1_4.setObjectName(_fromUtf8("textLabel1_4"))
        self.gridLayout_2.addWidget(self.textLabel1_4, 0, 0, 1, 2)
        self.groupBox = QtGui.QGroupBox(self.Commande)
        self.groupBox.setMinimumSize(QtCore.QSize(171, 71))
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.gridLayout1 = QtGui.QGridLayout(self.groupBox)
        self.gridLayout1.setObjectName(_fromUtf8("gridLayout1"))
        self.RBalpha = QtGui.QRadioButton(self.groupBox)
        self.RBalpha.setMinimumSize(QtCore.QSize(149, 16))
        self.RBalpha.setMaximumSize(QtCore.QSize(149, 16))
        self.RBalpha.setChecked(True)
        self.RBalpha.setObjectName(_fromUtf8("RBalpha"))
        self.gridLayout1.addWidget(self.RBalpha, 0, 0, 1, 1)
        self.RBGroupe = QtGui.QRadioButton(self.groupBox)
        self.RBGroupe.setObjectName(_fromUtf8("RBGroupe"))
        self.gridLayout1.addWidget(self.RBGroupe, 1, 0, 1, 1)
        self.gridLayout_2.addWidget(self.groupBox, 0, 4, 2, 1)
        self.textLabel6 = QtGui.QLabel(self.Commande)
        self.textLabel6.setMinimumSize(QtCore.QSize(91, 30))
        self.textLabel6.setWordWrap(False)
        self.textLabel6.setObjectName(_fromUtf8("textLabel6"))
        self.gridLayout_2.addWidget(self.textLabel6, 1, 0, 1, 1)
        self.LEFiltre = QtGui.QLineEdit(self.Commande)
        self.LEFiltre.setMinimumSize(QtCore.QSize(231, 30))
        self.LEFiltre.setObjectName(_fromUtf8("LEFiltre"))
        self.gridLayout_2.addWidget(self.LEFiltre, 1, 1, 1, 1)
        self.BNext = QtGui.QPushButton(self.Commande)
        self.BNext.setMinimumSize(QtCore.QSize(90, 30))
        self.BNext.setObjectName(_fromUtf8("BNext"))
        self.gridLayout_2.addWidget(self.BNext, 1, 2, 1, 1)
        spacerItem = QtGui.QSpacerItem(108, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem, 1, 3, 1, 1)
        self.gridLayout_4.addLayout(self.gridLayout_2, 0, 0, 1, 1)
        self.LBNouvCommande = QtGui.QListWidget(self.Commande)
        self.LBNouvCommande.setObjectName(_fromUtf8("LBNouvCommande"))
        self.gridLayout_4.addWidget(self.LBNouvCommande, 1, 0, 1, 1)
        self.textLabel4 = QtGui.QLabel(self.Commande)
        self.textLabel4.setMaximumSize(QtCore.QSize(721, 20))
        self.textLabel4.setWordWrap(False)
        self.textLabel4.setObjectName(_fromUtf8("textLabel4"))
        self.gridLayout_4.addWidget(self.textLabel4, 2, 0, 1, 1)
        self.TWChoix.addTab(self.Commande, _fromUtf8(""))
        self.gridLayout_3.addWidget(self.TWChoix, 0, 0, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem1 = QtGui.QSpacerItem(268, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.bOk = QtGui.QPushButton(DComment)
        self.bOk.setMinimumSize(QtCore.QSize(160, 30))
        self.bOk.setMaximumSize(QtCore.QSize(160, 30))
        self.bOk.setAutoDefault(True)
        self.bOk.setDefault(True)
        self.bOk.setObjectName(_fromUtf8("bOk"))
        self.horizontalLayout.addWidget(self.bOk)
        spacerItem2 = QtGui.QSpacerItem(328, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem2)
        self.gridLayout_3.addLayout(self.horizontalLayout, 1, 0, 1, 1)

        self.retranslateUi(DComment)
        self.TWChoix.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(DComment)
        DComment.setTabOrder(self.TWChoix, self.bOk)

    def retranslateUi(self, DComment):
        DComment.setWindowTitle(QtGui.QApplication.translate("DComment", "DComm", None, QtGui.QApplication.UnicodeUTF8))
        self.TWChoix.setTabText(self.TWChoix.indexOf(self.Valeur_Parametre), QtGui.QApplication.translate("DComment", "Commentaire", None, QtGui.QApplication.UnicodeUTF8))
        self.textLabel1_4.setText(QtGui.QApplication.translate("DComment", "<b><u>Commandes :</u></b>", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox.setTitle(QtGui.QApplication.translate("DComment", "Affichage", None, QtGui.QApplication.UnicodeUTF8))
        self.RBalpha.setText(QtGui.QApplication.translate("DComment", "alphabétique", None, QtGui.QApplication.UnicodeUTF8))
        self.RBGroupe.setText(QtGui.QApplication.translate("DComment", "par groupe", None, QtGui.QApplication.UnicodeUTF8))
        self.textLabel6.setText(QtGui.QApplication.translate("DComment", "Filtre", None, QtGui.QApplication.UnicodeUTF8))
        self.BNext.setText(QtGui.QApplication.translate("DComment", "Suivant", None, QtGui.QApplication.UnicodeUTF8))
        self.textLabel4.setText(QtGui.QApplication.translate("DComment", "La commande choisie sera ajoutée APRES la commande courante", None, QtGui.QApplication.UnicodeUTF8))
        self.TWChoix.setTabText(self.TWChoix.indexOf(self.Commande), QtGui.QApplication.translate("DComment", "Nouvelle Commande", None, QtGui.QApplication.UnicodeUTF8))
        self.bOk.setToolTip(QtGui.QApplication.translate("DComment", "validation de la saisie", None, QtGui.QApplication.UnicodeUTF8))
        self.bOk.setText(QtGui.QApplication.translate("DComment", "&Valider", None, QtGui.QApplication.UnicodeUTF8))
        self.bOk.setShortcut(QtGui.QApplication.translate("DComment", "Shift+A, Alt+A, Alt+A, Alt+A", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    DComment = QtGui.QWidget()
    ui = Ui_DComment()
    ui.setupUi(DComment)
    DComment.show()
    sys.exit(app.exec_())


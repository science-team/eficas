# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/local01/salome/V6_4_0rc2/tools/src/Eficasv1-V6_4_0rc2-py266-qt463p1-sip4112-pyqt481-cm285/UiQT4/desChoixCode.ui'
#
# Created: Mon Dec  5 18:57:13 2011
#      by: PyQt4 UI code generator 4.8.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_ChoixCode(object):
    def setupUi(self, ChoixCode):
        ChoixCode.setObjectName(_fromUtf8("ChoixCode"))
        ChoixCode.resize(508, 442)
        self.gridLayout_2 = QtGui.QGridLayout(ChoixCode)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label_choix = QtGui.QLabel(ChoixCode)
        self.label_choix.setMinimumSize(QtCore.QSize(0, 30))
        self.label_choix.setObjectName(_fromUtf8("label_choix"))
        self.horizontalLayout.addWidget(self.label_choix)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.gridLayout_2.addLayout(self.horizontalLayout, 0, 0, 1, 2)
        spacerItem1 = QtGui.QSpacerItem(20, 70, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Preferred)
        self.gridLayout_2.addItem(spacerItem1, 1, 0, 1, 1)
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem2, 0, 0, 1, 1)
        self.widget = QtGui.QWidget(ChoixCode)
        self.widget.setObjectName(_fromUtf8("widget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.widget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.rB_Aster = QtGui.QRadioButton(self.widget)
        self.rB_Aster.setMinimumSize(QtCore.QSize(0, 30))
        self.rB_Aster.setChecked(True)
        self.rB_Aster.setObjectName(_fromUtf8("rB_Aster"))
        self.verticalLayout.addWidget(self.rB_Aster)
        self.rB_Carmel3D = QtGui.QRadioButton(self.widget)
        self.rB_Carmel3D.setMinimumSize(QtCore.QSize(0, 30))
        self.rB_Carmel3D.setObjectName(_fromUtf8("rB_Carmel3D"))
        self.verticalLayout.addWidget(self.rB_Carmel3D)
        self.rB_Cuve2dg = QtGui.QRadioButton(self.widget)
        self.rB_Cuve2dg.setMinimumSize(QtCore.QSize(0, 30))
        self.rB_Cuve2dg.setObjectName(_fromUtf8("rB_Cuve2dg"))
        self.verticalLayout.addWidget(self.rB_Cuve2dg)
        self.rB_Carmel3D1 = QtGui.QRadioButton(self.widget)
        self.rB_Carmel3D1.setMinimumSize(QtCore.QSize(0, 30))
        self.rB_Carmel3D1.setObjectName(_fromUtf8("rB_Carmel3D1"))
        self.verticalLayout.addWidget(self.rB_Carmel3D1)
        self.rB_Openturns_Study = QtGui.QRadioButton(self.widget)
        self.rB_Openturns_Study.setMinimumSize(QtCore.QSize(0, 30))
        self.rB_Openturns_Study.setObjectName(_fromUtf8("rB_Openturns_Study"))
        self.verticalLayout.addWidget(self.rB_Openturns_Study)
        self.rB_Openturns_Wrapper = QtGui.QRadioButton(self.widget)
        self.rB_Openturns_Wrapper.setMinimumSize(QtCore.QSize(0, 30))
        self.rB_Openturns_Wrapper.setObjectName(_fromUtf8("rB_Openturns_Wrapper"))
        self.verticalLayout.addWidget(self.rB_Openturns_Wrapper)
        self.gridLayout.addWidget(self.widget, 0, 1, 1, 1)
        spacerItem3 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem3, 0, 2, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 2, 0, 1, 2)
        spacerItem4 = QtGui.QSpacerItem(20, 58, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Preferred)
        self.gridLayout_2.addItem(spacerItem4, 3, 1, 1, 1)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.pB_OK = QtGui.QPushButton(ChoixCode)
        self.pB_OK.setMinimumSize(QtCore.QSize(80, 30))
        self.pB_OK.setObjectName(_fromUtf8("pB_OK"))
        self.horizontalLayout_2.addWidget(self.pB_OK)
        spacerItem5 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem5)
        self.pB_cancel = QtGui.QPushButton(ChoixCode)
        self.pB_cancel.setMinimumSize(QtCore.QSize(80, 30))
        self.pB_cancel.setObjectName(_fromUtf8("pB_cancel"))
        self.horizontalLayout_2.addWidget(self.pB_cancel)
        self.gridLayout_2.addLayout(self.horizontalLayout_2, 4, 0, 1, 2)

        self.retranslateUi(ChoixCode)
        QtCore.QMetaObject.connectSlotsByName(ChoixCode)

    def retranslateUi(self, ChoixCode):
        ChoixCode.setWindowTitle(QtGui.QApplication.translate("ChoixCode", "Choix du code", None, QtGui.QApplication.UnicodeUTF8))
        self.label_choix.setText(QtGui.QApplication.translate("ChoixCode", "Veuillez choisir un code :", None, QtGui.QApplication.UnicodeUTF8))
        self.rB_Aster.setText(QtGui.QApplication.translate("ChoixCode", "Aster", None, QtGui.QApplication.UnicodeUTF8))
        self.rB_Carmel3D.setText(QtGui.QApplication.translate("ChoixCode", "Carmel3D", None, QtGui.QApplication.UnicodeUTF8))
        self.rB_Cuve2dg.setText(QtGui.QApplication.translate("ChoixCode", "Cuve2dg", None, QtGui.QApplication.UnicodeUTF8))
        self.rB_Carmel3D1.setText(QtGui.QApplication.translate("ChoixCode", "MAP", None, QtGui.QApplication.UnicodeUTF8))
        self.rB_Openturns_Study.setText(QtGui.QApplication.translate("ChoixCode", "Openturns_Study", None, QtGui.QApplication.UnicodeUTF8))
        self.rB_Openturns_Wrapper.setText(QtGui.QApplication.translate("ChoixCode", "Openturns_Wrapper", None, QtGui.QApplication.UnicodeUTF8))
        self.pB_OK.setText(QtGui.QApplication.translate("ChoixCode", "OK", None, QtGui.QApplication.UnicodeUTF8))
        self.pB_cancel.setText(QtGui.QApplication.translate("ChoixCode", "Cancel", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    ChoixCode = QtGui.QWidget()
    ui = Ui_ChoixCode()
    ui.setupUi(ChoixCode)
    ChoixCode.show()
    sys.exit(app.exec_())


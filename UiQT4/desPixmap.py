# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/local01/salome/V6_4_0rc2/tools/src/Eficasv1-V6_4_0rc2-py266-qt463p1-sip4112-pyqt481-cm285/UiQT4/desPixmap.ui'
#
# Created: Mon Dec  5 18:57:13 2011
#      by: PyQt4 UI code generator 4.8.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_LabelPixmap(object):
    def setupUi(self, LabelPixmap):
        LabelPixmap.setObjectName(_fromUtf8("LabelPixmap"))
        LabelPixmap.resize(400, 300)
        self.gridLayout = QtGui.QGridLayout(LabelPixmap)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.labelPix = QtGui.QLabel(LabelPixmap)
        self.labelPix.setText(_fromUtf8(""))
        self.labelPix.setObjectName(_fromUtf8("labelPix"))
        self.gridLayout.addWidget(self.labelPix, 0, 0, 1, 1)

        self.retranslateUi(LabelPixmap)
        QtCore.QMetaObject.connectSlotsByName(LabelPixmap)

    def retranslateUi(self, LabelPixmap):
        LabelPixmap.setWindowTitle(QtGui.QApplication.translate("LabelPixmap", "PDF de la loi", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    LabelPixmap = QtGui.QDialog()
    ui = Ui_LabelPixmap()
    ui.setupUi(LabelPixmap)
    LabelPixmap.show()
    sys.exit(app.exec_())


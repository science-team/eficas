# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/local01/salome/V6_4_0rc2/tools/src/Eficasv1-V6_4_0rc2-py266-qt463p1-sip4112-pyqt481-cm285/UiQT4/desUniqueInto.ui'
#
# Created: Mon Dec  5 18:57:13 2011
#      by: PyQt4 UI code generator 4.8.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_DUnIn(object):
    def setupUi(self, DUnIn):
        DUnIn.setObjectName(_fromUtf8("DUnIn"))
        DUnIn.resize(482, 480)
        DUnIn.setMinimumSize(QtCore.QSize(350, 0))
        self.gridLayout_2 = QtGui.QGridLayout(DUnIn)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.tabuniqueinto = QtGui.QTabWidget(DUnIn)
        self.tabuniqueinto.setObjectName(_fromUtf8("tabuniqueinto"))
        self.Widget8 = QtGui.QWidget()
        self.Widget8.setObjectName(_fromUtf8("Widget8"))
        self.gridLayout = QtGui.QGridLayout(self.Widget8)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.textLabel2 = QtGui.QLabel(self.Widget8)
        self.textLabel2.setWordWrap(False)
        self.textLabel2.setObjectName(_fromUtf8("textLabel2"))
        self.gridLayout.addWidget(self.textLabel2, 0, 0, 1, 1)
        self.listBoxVal = QtGui.QListWidget(self.Widget8)
        self.listBoxVal.setObjectName(_fromUtf8("listBoxVal"))
        self.gridLayout.addWidget(self.listBoxVal, 1, 0, 1, 1)
        self.Commentaire = QtGui.QLabel(self.Widget8)
        self.Commentaire.setMinimumSize(QtCore.QSize(420, 30))
        self.Commentaire.setText(_fromUtf8(""))
        self.Commentaire.setWordWrap(False)
        self.Commentaire.setObjectName(_fromUtf8("Commentaire"))
        self.gridLayout.addWidget(self.Commentaire, 2, 0, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem = QtGui.QSpacerItem(118, 27, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.bOk = QtGui.QPushButton(self.Widget8)
        self.bOk.setMinimumSize(QtCore.QSize(160, 30))
        self.bOk.setMaximumSize(QtCore.QSize(160, 30))
        self.bOk.setAutoDefault(True)
        self.bOk.setDefault(True)
        self.bOk.setObjectName(_fromUtf8("bOk"))
        self.horizontalLayout.addWidget(self.bOk)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.BAlpha = QtGui.QPushButton(self.Widget8)
        self.BAlpha.setObjectName(_fromUtf8("BAlpha"))
        self.horizontalLayout.addWidget(self.BAlpha)
        self.gridLayout.addLayout(self.horizontalLayout, 3, 0, 1, 1)
        self.tabuniqueinto.addTab(self.Widget8, _fromUtf8(""))
        self.gridLayout_2.addWidget(self.tabuniqueinto, 0, 0, 1, 1)

        self.retranslateUi(DUnIn)
        QtCore.QMetaObject.connectSlotsByName(DUnIn)

    def retranslateUi(self, DUnIn):
        DUnIn.setWindowTitle(QtGui.QApplication.translate("DUnIn", "DUnIn", None, QtGui.QApplication.UnicodeUTF8))
        self.textLabel2.setText(QtGui.QApplication.translate("DUnIn", "<b><u><p align=\"center\">Valeurs possibles</p></u></b>", None, QtGui.QApplication.UnicodeUTF8))
        self.bOk.setToolTip(QtGui.QApplication.translate("DUnIn", "validation de la saisie", None, QtGui.QApplication.UnicodeUTF8))
        self.bOk.setText(QtGui.QApplication.translate("DUnIn", "&Valider", None, QtGui.QApplication.UnicodeUTF8))
        self.bOk.setShortcut(QtGui.QApplication.translate("DUnIn", "Shift+A, Alt+A, Alt+A, Alt+A", None, QtGui.QApplication.UnicodeUTF8))
        self.BAlpha.setText(QtGui.QApplication.translate("DUnIn", "Tri alpha", None, QtGui.QApplication.UnicodeUTF8))
        self.tabuniqueinto.setTabText(self.tabuniqueinto.indexOf(self.Widget8), QtGui.QApplication.translate("DUnIn", "Saisir Valeur", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    DUnIn = QtGui.QWidget()
    ui = Ui_DUnIn()
    ui.setupUi(DUnIn)
    DUnIn.show()
    sys.exit(app.exec_())


# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/local01/salome/V6_4_0rc2/tools/src/Eficasv1-V6_4_0rc2-py266-qt463p1-sip4112-pyqt481-cm285/UiQT4/desUniqueBase.ui'
#
# Created: Mon Dec  5 18:57:13 2011
#      by: PyQt4 UI code generator 4.8.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_DUnBase(object):
    def setupUi(self, DUnBase):
        DUnBase.setObjectName(_fromUtf8("DUnBase"))
        DUnBase.resize(482, 480)
        self.gridlayout = QtGui.QGridLayout(DUnBase)
        self.gridlayout.setObjectName(_fromUtf8("gridlayout"))
        self.tabuniqueinto = QtGui.QTabWidget(DUnBase)
        self.tabuniqueinto.setObjectName(_fromUtf8("tabuniqueinto"))
        self.Widget8 = QtGui.QWidget()
        self.Widget8.setObjectName(_fromUtf8("Widget8"))
        self.gridLayout_2 = QtGui.QGridLayout(self.Widget8)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.textLabel2 = QtGui.QLabel(self.Widget8)
        self.textLabel2.setWordWrap(False)
        self.textLabel2.setObjectName(_fromUtf8("textLabel2"))
        self.horizontalLayout.addWidget(self.textLabel2)
        self.lineEditVal = QtGui.QLineEdit(self.Widget8)
        self.lineEditVal.setMinimumSize(QtCore.QSize(290, 50))
        self.lineEditVal.setObjectName(_fromUtf8("lineEditVal"))
        self.horizontalLayout.addWidget(self.lineEditVal)
        self.gridLayout_2.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        spacerItem = QtGui.QSpacerItem(293, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 0, 0, 1, 1)
        self.BSalome = QtGui.QPushButton(self.Widget8)
        self.BSalome.setMinimumSize(QtCore.QSize(142, 40))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("image240.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.BSalome.setIcon(icon)
        self.BSalome.setObjectName(_fromUtf8("BSalome"))
        self.gridLayout.addWidget(self.BSalome, 0, 1, 1, 1)
        self.bParametres = QtGui.QPushButton(self.Widget8)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.bParametres.sizePolicy().hasHeightForWidth())
        self.bParametres.setSizePolicy(sizePolicy)
        self.bParametres.setMinimumSize(QtCore.QSize(140, 45))
        self.bParametres.setObjectName(_fromUtf8("bParametres"))
        self.gridLayout.addWidget(self.bParametres, 0, 2, 1, 1)
        spacerItem1 = QtGui.QSpacerItem(138, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 1, 0, 1, 1)
        self.BView2D = QtGui.QPushButton(self.Widget8)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.BView2D.sizePolicy().hasHeightForWidth())
        self.BView2D.setSizePolicy(sizePolicy)
        self.BView2D.setMinimumSize(QtCore.QSize(140, 40))
        self.BView2D.setObjectName(_fromUtf8("BView2D"))
        self.gridLayout.addWidget(self.BView2D, 1, 1, 1, 1)
        self.BFichier = QtGui.QPushButton(self.Widget8)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.BFichier.sizePolicy().hasHeightForWidth())
        self.BFichier.setSizePolicy(sizePolicy)
        self.BFichier.setMinimumSize(QtCore.QSize(140, 40))
        self.BFichier.setIcon(icon)
        self.BFichier.setObjectName(_fromUtf8("BFichier"))
        self.gridLayout.addWidget(self.BFichier, 1, 2, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 1, 0, 1, 1)
        self.Commentaire = QtGui.QLabel(self.Widget8)
        self.Commentaire.setObjectName(_fromUtf8("Commentaire"))
        self.gridLayout_2.addWidget(self.Commentaire, 2, 0, 1, 1)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem2)
        self.bOk = QtGui.QPushButton(self.Widget8)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.bOk.sizePolicy().hasHeightForWidth())
        self.bOk.setSizePolicy(sizePolicy)
        self.bOk.setMinimumSize(QtCore.QSize(140, 40))
        self.bOk.setObjectName(_fromUtf8("bOk"))
        self.horizontalLayout_2.addWidget(self.bOk)
        spacerItem3 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem3)
        self.gridLayout_2.addLayout(self.horizontalLayout_2, 3, 0, 1, 1)
        self.tabuniqueinto.addTab(self.Widget8, _fromUtf8(""))
        self.gridlayout.addWidget(self.tabuniqueinto, 0, 0, 1, 1)

        self.retranslateUi(DUnBase)
        QtCore.QMetaObject.connectSlotsByName(DUnBase)

    def retranslateUi(self, DUnBase):
        DUnBase.setWindowTitle(QtGui.QApplication.translate("DUnBase", "DUnIn", None, QtGui.QApplication.UnicodeUTF8))
        self.textLabel2.setText(QtGui.QApplication.translate("DUnBase", "<b><u><p align=\"center\">Valeur: </p></u></b>", None, QtGui.QApplication.UnicodeUTF8))
        self.BSalome.setText(QtGui.QApplication.translate("DUnBase", "Selectionner", None, QtGui.QApplication.UnicodeUTF8))
        self.bParametres.setText(QtGui.QApplication.translate("DUnBase", "Parametres", None, QtGui.QApplication.UnicodeUTF8))
        self.BView2D.setText(QtGui.QApplication.translate("DUnBase", "Visualiser", None, QtGui.QApplication.UnicodeUTF8))
        self.BFichier.setText(QtGui.QApplication.translate("DUnBase", "Fichier", None, QtGui.QApplication.UnicodeUTF8))
        self.Commentaire.setText(QtGui.QApplication.translate("DUnBase", "TextLabel", None, QtGui.QApplication.UnicodeUTF8))
        self.bOk.setToolTip(QtGui.QApplication.translate("DUnBase", "validation de la saisie", None, QtGui.QApplication.UnicodeUTF8))
        self.bOk.setText(QtGui.QApplication.translate("DUnBase", "&Valider", None, QtGui.QApplication.UnicodeUTF8))
        self.bOk.setShortcut(QtGui.QApplication.translate("DUnBase", "Shift+A, Alt+A, Alt+A, Alt+A", None, QtGui.QApplication.UnicodeUTF8))
        self.tabuniqueinto.setTabText(self.tabuniqueinto.indexOf(self.Widget8), QtGui.QApplication.translate("DUnBase", "Saisir Valeur", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    DUnBase = QtGui.QWidget()
    ui = Ui_DUnBase()
    ui.setupUi(DUnBase)
    DUnBase.show()
    sys.exit(app.exec_())


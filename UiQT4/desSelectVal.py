# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/local01/salome/V6_4_0rc2/tools/src/Eficasv1-V6_4_0rc2-py266-qt463p1-sip4112-pyqt481-cm285/UiQT4/desSelectVal.ui'
#
# Created: Mon Dec  5 18:57:13 2011
#      by: PyQt4 UI code generator 4.8.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_DSelVal(object):
    def setupUi(self, DSelVal):
        DSelVal.setObjectName(_fromUtf8("DSelVal"))
        DSelVal.resize(468, 610)
        self.verticalLayout_3 = QtGui.QVBoxLayout(DSelVal)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.TBtext = QtGui.QTextEdit(DSelVal)
        self.TBtext.setMinimumSize(QtCore.QSize(0, 400))
        self.TBtext.setObjectName(_fromUtf8("TBtext"))
        self.verticalLayout_3.addWidget(self.TBtext)
        spacerItem = QtGui.QSpacerItem(20, 73, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.BGSeparateur = QtGui.QGroupBox(DSelVal)
        self.BGSeparateur.setObjectName(_fromUtf8("BGSeparateur"))
        self.verticalLayout = QtGui.QVBoxLayout(self.BGSeparateur)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.Bespace = QtGui.QRadioButton(self.BGSeparateur)
        self.Bespace.setChecked(True)
        self.Bespace.setObjectName(_fromUtf8("Bespace"))
        self.verticalLayout.addWidget(self.Bespace)
        self.Bvirgule = QtGui.QRadioButton(self.BGSeparateur)
        self.Bvirgule.setObjectName(_fromUtf8("Bvirgule"))
        self.verticalLayout.addWidget(self.Bvirgule)
        self.BpointVirgule = QtGui.QRadioButton(self.BGSeparateur)
        self.BpointVirgule.setObjectName(_fromUtf8("BpointVirgule"))
        self.verticalLayout.addWidget(self.BpointVirgule)
        self.horizontalLayout.addWidget(self.BGSeparateur)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        spacerItem2 = QtGui.QSpacerItem(17, 13, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem2)
        self.BImportSel = QtGui.QPushButton(DSelVal)
        self.BImportSel.setObjectName(_fromUtf8("BImportSel"))
        self.verticalLayout_2.addWidget(self.BImportSel)
        self.BImportTout = QtGui.QPushButton(DSelVal)
        self.BImportTout.setObjectName(_fromUtf8("BImportTout"))
        self.verticalLayout_2.addWidget(self.BImportTout)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.verticalLayout_3.addLayout(self.horizontalLayout)

        self.retranslateUi(DSelVal)
        QtCore.QMetaObject.connectSlotsByName(DSelVal)

    def retranslateUi(self, DSelVal):
        DSelVal.setWindowTitle(QtGui.QApplication.translate("DSelVal", "Sélection de valeurs", None, QtGui.QApplication.UnicodeUTF8))
        self.BGSeparateur.setTitle(QtGui.QApplication.translate("DSelVal", "Separateur", None, QtGui.QApplication.UnicodeUTF8))
        self.Bespace.setText(QtGui.QApplication.translate("DSelVal", "espace", None, QtGui.QApplication.UnicodeUTF8))
        self.Bvirgule.setText(QtGui.QApplication.translate("DSelVal", "virgule", None, QtGui.QApplication.UnicodeUTF8))
        self.BpointVirgule.setText(QtGui.QApplication.translate("DSelVal", "point-virgule", None, QtGui.QApplication.UnicodeUTF8))
        self.BImportSel.setText(QtGui.QApplication.translate("DSelVal", "Ajouter Selection", None, QtGui.QApplication.UnicodeUTF8))
        self.BImportTout.setText(QtGui.QApplication.translate("DSelVal", "Importer Tout", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    DSelVal = QtGui.QWidget()
    ui = Ui_DSelVal()
    ui.setupUi(DSelVal)
    DSelVal.show()
    sys.exit(app.exec_())


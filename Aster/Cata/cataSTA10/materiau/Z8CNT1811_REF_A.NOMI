# MODIF  DATE 24/10/2006   AUTEUR DURAND C.DURAND 
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2001  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR   
# (AT YOUR OPTION) ANY LATER VERSION.                                 
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT 
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF          
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU    
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                            
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE   
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,       
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.      
# ======================================================================
#------------------------------------------------------
# LAMBDA EN FONCTION DE LA TEMPéRATURE
#
coef0=1.E ## UNIT -3
_A0=DEFI_FONCTION( 
          NOM_PARA='TEMP',
       PROL_DROITE='EXCLU',
       PROL_GAUCHE='EXCLU',
              VALE=(
          20.            ,14.7            *coef0,
          50.            ,15.2            *coef0,
          100.           ,15.8            *coef0,
          150.           ,16.7            *coef0,
          200.           ,17.2            *coef0,
          250.           ,18.0            *coef0,
          300.           ,18.6            *coef0,
          350.           ,19.3            *coef0,
          400.           ,20.0            *coef0,
          450.           ,20.5            *coef0,
          500.           ,21.1            *coef0,
          550.           ,21.7            *coef0,
          600.           ,22.2            *coef0,
          650.           ,22.7            *coef0,
          700.           ,23.2            *coef0,
          750.           ,23.7            *coef0,
          800.           ,24.1            *coef0,
          ))
 
#
# RHO_CP EN FONCTION DE LA TEMPéRATURE
#
coef1=1.E ## UNIT -9
_A1=DEFI_FONCTION( 
           NOM_PARA='TEMP',
        PROL_DROITE='EXCLU',
        PROL_GAUCHE='EXCLU',
               VALE=(
          20.            ,3.60E+6         *coef1,
          50.            ,3.74E+6         *coef1,
          100.           ,3.90E+6         *coef1,
          150.           ,4.10E+6         *coef1,
          200.           ,4.16E+6         *coef1,
          250.           ,4.27E+6         *coef1,
          300.           ,4.30E+6         *coef1,
          350.           ,4.35E+6         *coef1,
          400.           ,4.39E+6         *coef1,
          450.           ,4.39E+6         *coef1,
          500.           ,4.44E+6         *coef1,
          550.           ,4.47E+6         *coef1,
          600.           ,4.49E+6         *coef1,
          650.           ,4.53E+6         *coef1,
          700.           ,4.58E+6         *coef1,
          750.           ,4.64E+6         *coef1,
          800.           ,4.66E+6         *coef1,
          ))
 
#
# E EN FONCTION DE LA TEMPéRATURE
#
coef2=1.E ## UNIT -6
_A2=DEFI_FONCTION( 
            NOM_PARA='TEMP',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
                VALE=(
          0.             ,198.5E+9        *coef2,
          20.            ,197.0E+9        *coef2,
          50.            ,195.0E+9        *coef2,
          100.           ,191.5E+9        *coef2,
          150.           ,187.5E+9        *coef2,
          200.           ,184.0E+9        *coef2,
          250.           ,180.0E+9        *coef2,
          300.           ,176.5E+9        *coef2,
          350.           ,172.0E+9        *coef2,
          400.           ,168.0E+9        *coef2,
          450.           ,164.0E+9        *coef2,
          500.           ,160.0E+9        *coef2,
          550.           ,155.5E+9        *coef2,
          600.           ,151.5E+9        *coef2,
          ))
 
#
# NU EN FONCTION DE LA TEMPéRATURE
#

_A3=DEFI_FONCTION( 
            NOM_PARA='TEMP',
         PROL_DROITE='CONSTANT',
         PROL_GAUCHE='CONSTANT',
                VALE=(   0.,    0.3,
                     ))

#
# ALPHA EN FONCTION DE LA TEMPéRATURE
#

_A4=DEFI_FONCTION( 
            NOM_PARA='TEMP',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
                VALE=(   20., 16.40E-6,     50.,  16.54E-6,
                        100., 16.80E-6,    150.,  17.04E-6,
                        200., 17.20E-6,    250.,  17.50E-6,
                        300., 17.70E-6,    350.,  17.90E-6,
                        400., 18.10E-6,    450.,  18.24E-6,
                     ))


# COURBE DE FATIGUE DE WOHLER RCCM Z1.4.2

_AZ=DEFI_FONCTION(NOM_PARA='SIGM',
                     INTERPOL='LOG',
                     VALE=(
                     180.0E6,   1000000.0  ,
                     200.0E6,   500000.0  ,
                     230.0E6,   200000.0  ,
                     260.0E6,   100000.0  ,
                     295.0E6,   50000.0  ,
                     350.0E6,   20000.0  ,
                     405.0E6,   10000.0  ,
                     485.0E6,   5000.0  ,
                     615.0E6,   2000.0  ,
                     750.0E6,   1000.0  ,
                     940.0E6,   500.0  ,
                     1275.0E6,   200.0  ,
                     1655.0E6,   100.0  ,
                     2190.0E6,   50.0  ,
                     3240.0E6,   20.0  ,
                     4480.0E6,   10.0,
                          ),);
_BZ=CALC_FONCTION(INVERSE=_F(FONCTION=_AZ,),);
_CZ=CALC_FONCTION(COMB=_F(FONCTION = _BZ,
                         COEF     =  1.E ## UNIT -6
                         ),); 
DETRUIRE( CONCEPT =_F( NOM=_AZ), )
_AZ=CALC_FONCTION(INVERSE=_F(FONCTION=_CZ,),);




MAT=DEFI_MATERIAU( 
              THER_NL=_F(                ## SUBST THER
               RHO_CP = _A1,             ## EVAL _A1
               LAMBDA = _A0,             ## EVAL _A0
               ),
              ELAS_FO=_F(                ## SUBST ELAS
               E = _A2,                  ## EVAL _A2
               NU = _A3,                 ## EVAL _A3
               ALPHA = _A4,              ## EVAL _A4
               TEMP_DEF_ALPHA = 20.,     ## SUPPR
               ),
               
             FATIGUE=_F(WOHLER=_AZ,
                         E_REFE=1.79E11*1.E ## UNIT -6
                        ),
              
          )

#


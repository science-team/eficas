# MODIF  DATE 30/05/2011   AUTEUR COURTOIS M.COURTOIS 
# -*- coding: iso-8859-1 -*-
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2001  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
#------------------------------------------------------
# LAMBDA EN FONCTION DE LA TEMPéRATURE
#
coef0=coef_unit(-3)
_A0=DEFI_FONCTION(
          NOM_PARA='TEMP',
       PROL_DROITE='EXCLU',
       PROL_GAUCHE='EXCLU',
              VALE=(
          20.            ,14.0            *coef0,
          50.            ,14.4            *coef0,
          100.           ,15.2            *coef0,
          150.           ,15.8            *coef0,
          200.           ,16.6            *coef0,
          250.           ,17.3            *coef0,
          300.           ,17.9            *coef0,
          350.           ,18.6            *coef0,
          400.           ,19.2            *coef0,
          450.           ,19.9            *coef0,
          500.           ,20.6            *coef0,
          550.           ,21.2            *coef0,
          600.           ,21.8            *coef0,
          650.           ,22.4            *coef0,
          700.           ,23.1            *coef0,
          750.           ,23.7            *coef0,
          800.           ,24.3            *coef0,
          ))

#
# RHO_CP EN FONCTION DE LA TEMPéRATURE
#
coef1=coef_unit(-9)
_A1=DEFI_FONCTION(
           NOM_PARA='TEMP',
        PROL_DROITE='EXCLU',
        PROL_GAUCHE='EXCLU',
               VALE=(
          20.            ,3.60E+6         *coef1,
          50.            ,3.70E+6         *coef1,
          100.           ,3.91E+6         *coef1,
          150.           ,4.01E+6         *coef1,
          200.           ,4.16E+6         *coef1,
          250.           ,4.26E+6         *coef1,
          300.           ,4.29E+6         *coef1,
          350.           ,4.37E+6         *coef1,
          400.           ,4.39E+6         *coef1,
          450.           ,4.42E+6         *coef1,
          500.           ,4.44E+6         *coef1,
          550.           ,4.46E+6         *coef1,
          600.           ,4.49E+6         *coef1,
          650.           ,4.53E+6         *coef1,
          700.           ,4.58E+6         *coef1,
          750.           ,4.64E+6         *coef1,
          800.           ,4.70E+6         *coef1,
          ))

#
# E EN FONCTION DE LA TEMPéRATURE
#
coef2=coef_unit(-6)
_A2=DEFI_FONCTION(
            NOM_PARA='TEMP',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
                VALE=(
          0.             ,198.5E+9        *coef2,
          20.            ,197.0E+9        *coef2,
          50.            ,195.0E+9        *coef2,
          100.           ,191.5E+9        *coef2,
          150.           ,187.5E+9        *coef2,
          200.           ,184.0E+9        *coef2,
          250.           ,180.0E+9        *coef2,
          300.           ,176.5E+9        *coef2,
          350.           ,172.0E+9        *coef2,
          400.           ,168.0E+9        *coef2,
          450.           ,164.0E+9        *coef2,
          500.           ,160.0E+9        *coef2,
          550.           ,155.5E+9        *coef2,
          600.           ,151.5E+9        *coef2,
          ))

#
# NU EN FONCTION DE LA TEMPéRATURE
#

_A3=DEFI_FONCTION(
            NOM_PARA='TEMP',
         PROL_DROITE='CONSTANT',
         PROL_GAUCHE='CONSTANT',
                VALE=(   0.,    0.3,
                     ))

#
# ALPHA EN FONCTION DE LA TEMPéRATURE
#

_A4=DEFI_FONCTION(
            NOM_PARA='TEMP',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
                VALE=(   20., 15.54E-6,     50.,  15.72E-6,
                        100., 16.00E-6,    150.,  16.30E-6,
                        200., 16.60E-6,    250.,  16.86E-6,
                        300., 17.10E-6,    350.,  17.36E-6,
                        400., 17.60E-6,    450.,  17.82E-6,
                     ))


# COURBE DE FATIGUE DE WOHLER RCCM Z1.4.2

_AZ=DEFI_FONCTION(NOM_PARA='SIGM',
                     INTERPOL='LOG',
                     VALE=(
                     180.0E6,   1000000.0  ,
                     200.0E6,   500000.0  ,
                     230.0E6,   200000.0  ,
                     260.0E6,   100000.0  ,
                     295.0E6,   50000.0  ,
                     350.0E6,   20000.0  ,
                     405.0E6,   10000.0  ,
                     485.0E6,   5000.0  ,
                     615.0E6,   2000.0  ,
                     750.0E6,   1000.0  ,
                     940.0E6,   500.0  ,
                     1275.0E6,   200.0  ,
                     1655.0E6,   100.0  ,
                     2190.0E6,   50.0  ,
                     3240.0E6,   20.0  ,
                     4480.0E6,   10.0,
                          ),);
_BZ=CALC_FONCTION(INVERSE=_F(FONCTION=_AZ,),);
_CZ=CALC_FONCTION(COMB=_F(FONCTION = _BZ,
                         COEF     =  coef_unit(-6)
                         ),);
DETRUIRE( CONCEPT =_F( NOM=_AZ), )
_AZ=CALC_FONCTION(INVERSE=_F(FONCTION=_CZ,),);




motscles = defi_motscles(
              THER=_F(
               extraction=True,
               RHO_CP  = temp_eval(_A1),
               LAMBDA  = temp_eval(_A0),
               ),
              THER_NL=_F(
               extraction=False,
               RHO_CP = _A1,
               LAMBDA = _A0,
               ),
              ELAS=_F(
               extraction=True,
               E  = temp_eval(_A2),
               NU  = temp_eval(_A3),
               ALPHA  = temp_eval(_A4),
               ),
              ELAS_FO=_F(
               extraction=False,
               E = _A2,
               NU = _A3,
               ALPHA = _A4,
               TEMP_DEF_ALPHA = 20.,
               ),

             FATIGUE=_F(WOHLER=_AZ,
                         E_REFE=1.79E11*coef_unit(-6)
                        ),

          )

#


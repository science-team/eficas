# MODIF  DATE 30/05/2011   AUTEUR COURTOIS M.COURTOIS 
# -*- coding: iso-8859-1 -*-
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2001  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
#------------------------------------------------------
# LAMBDA EN FONCTION DE LA TEMP�RATURE
#
coef0=coef_unit(-3)
_A0=DEFI_FONCTION(
          NOM_PARA='TEMP',
       PROL_DROITE='EXCLU',
       PROL_GAUCHE='EXCLU',
              VALE=(
          20.            ,37.7            *coef0,
          50.            ,38.6            *coef0,
          100.           ,39.9            *coef0,
          150.           ,40.5            *coef0,
          200.           ,40.5            *coef0,
          250.           ,40.2            *coef0,
          300.           ,39.5            *coef0,
          350.           ,38.7            *coef0,
          400.           ,37.7            *coef0,
          450.           ,36.6            *coef0,
          500.           ,35.5            *coef0,
          550.           ,34.3            *coef0,
          600.           ,33.0            *coef0,
          650.           ,31.8            *coef0,
          700.           ,30.68           *coef0,
          750.           ,29.28           *coef0,
          800.           ,27.9            *coef0,
          850.           ,26.56           *coef0,
          900.           ,25.27           *coef0,
          ))

#
# RHO_CP EN FONCTION DE LA TEMP�RATURE
#
coef1=coef_unit(-9)
_A1=DEFI_FONCTION(
           NOM_PARA='TEMP',
        PROL_DROITE='EXCLU',
        PROL_GAUCHE='EXCLU',
               VALE=(
          20.            ,3.49E+6         *coef1,
          50.            ,3.59E+6         *coef1,
          100.           ,3.77E+6         *coef1,
          150.           ,3.93E+6         *coef1,
          200.           ,4.09E+6         *coef1,
          250.           ,4.27E+6         *coef1,
          300.           ,4.42E+6         *coef1,
          350.           ,4.60E+6         *coef1,
          400.           ,4.80E+6         *coef1,
          450.           ,5.04E+6         *coef1,
          500.           ,5.35E+6         *coef1,
          550.           ,5.69E+6         *coef1,
          600.           ,6.10E+6         *coef1,
          650.           ,6.65E+6         *coef1,
          750.           ,10.13E+6        *coef1,
          800.           ,8.11E+6         *coef1,
          850.           ,6.73E+6         *coef1,
          900.           ,5.88E+6         *coef1,
          ))

#
# E EN FONCTION DE LA TEMP�RATURE
#
coef2=coef_unit(-6)
_A2=DEFI_FONCTION(
            NOM_PARA='TEMP',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
                VALE=(
          -170.          ,210.E+9         *coef2,
          0.             ,205.E+9         *coef2,
          20.            ,204.E+9         *coef2,
          50.            ,203.E+9         *coef2,
          100.           ,200.E+9         *coef2,
          150.           ,197.E+9         *coef2,
          200.           ,193.E+9         *coef2,
          250.           ,189.E+9         *coef2,
          300.           ,185.E+9         *coef2,
          350.           ,180.E+9         *coef2,
          400.           ,176.E+9         *coef2,
          450.           ,171.E+9         *coef2,
          500.           ,166.E+9         *coef2,
          550.           ,160.E+9         *coef2,
          600.           ,155.E+9         *coef2,
          ))

#
# NU EN FONCTION DE LA TEMP�RATURE
#
_A3=DEFI_FONCTION(
            NOM_PARA='TEMP',
         PROL_DROITE='CONSTANT',
         PROL_GAUCHE='CONSTANT',
                VALE=(   0.,    0.3,
                     ))

#
# ALPHA EN FONCTION DE LA TEMP�RATURE
#

_A4=DEFI_FONCTION(
            NOM_PARA='TEMP',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
                VALE=(   20., 11.22E-6,     50.,  11.45E-6,
                        100., 11.79E-6,    150.,  12.14E-6,
                        200., 12.47E-6,    250.,  12.78E-6,
                        300., 13.08E-6,    350.,  13.40E-6,
                        400., 13.72E-6,    450.,  14.02E-6,
                        500., 14.32E-6,    550.,  14.56E-6,
                        600., 14.80E-6,    650.,  15.02E-6,
                        700., 15.24E-6,    850.,  15.83E-6,
                        900., 16.01E-6,
                     ))

#
# COURBE DE TRACTION � LA TEMP�RATURE -170�C
#

coef5=coef_unit(-6)
_A5=DEFI_FONCTION(
            NOM_PARA='EPSI',
        PROL_DROITE='EXCLU',
        PROL_GAUCHE='EXCLU',
                VALE=(
          3.65710E-3,     768.E+6         *coef5,
          4.99952E-3,     779.E+6         *coef5,
          7.99952E-3,     800.E+6         *coef5,
          0.09530,        963.E+6         *coef5,
          0.199998,      1100.E+6         *coef5,
                     ))

#
# COURBE DE TRACTION � LA TEMP�RATURE 20�C
#
coef6=coef_unit(-6)
_A6=DEFI_FONCTION(
             NOM_PARA='EPSI',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
                 VALE=(
          2.20588E-3     ,450.E+6         *coef6,
          3.30392E-3     ,470.E+6         *coef6,
          4.37745E-3     ,485.E+6         *coef6,
          5.41176E-3     ,492.E+6         *coef6,
          6.43630E-3     ,497.E+6         *coef6,
          7.46078E-3     ,502.E+6         *coef6,
          0.012554       ,521.E+6         *coef6,
          0.017622       ,535.E+6         *coef6,
          0.0226985      ,550.5E+6        *coef6,
          0.027752       ,561.5E+6        *coef6,
          0.032819       ,575.E+6         *coef6,
          0.03788        ,588.E+6         *coef6,
          0.04293        ,598.E+6         *coef6,
          0.047985       ,609.E+6         *coef6,
          0.053034       ,619.E+6         *coef6,
          0.0580735      ,627.E+6         *coef6,
          0.063108       ,634.E+6         *coef6,
          0.0681456      ,641.7E+6        *coef6,
          0.073172       ,647.E+6         *coef6,
          0.078196       ,652.E+6         *coef6,
          0.0832206      ,657.E+6         *coef6,
          0.0882402      ,661.E+6         *coef6,
          0.0932647      ,666.E+6         *coef6,
          0.098284       ,670.E+6         *coef6,
          0.103294       ,672.E+6         *coef6,
          ))

#
# COURBE DE TRACTION � LA TEMP�RATURE 100�C
#
coef7=coef_unit(-6)
_A7=DEFI_FONCTION(
             NOM_PARA='EPSI',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
                 VALE=(
          2.185E-3       ,437.E+6         *coef7,
          9.185E-3       ,437.E+6         *coef7,
          0.010235       ,447.E+6         *coef7,
          0.015385       ,477.E+6         *coef7,
          0.020485       ,497.E+6         *coef7,
          0.025585       ,517.E+6         *coef7,
          0.030635       ,527.E+6         *coef7,
          0.03571        ,542.E+6         *coef7,
          0.04076        ,552.E+6         *coef7,
          0.04581        ,562.E+6         *coef7,
          0.050835       ,567.E+6         *coef7,
          0.05587        ,574.E+6         *coef7,
          0.06091        ,582.E+6         *coef7,
          0.065935       ,587.E+6         *coef7,
          0.07097        ,594.E+6         *coef7,
          0.075995       ,599.E+6         *coef7,
          0.081035       ,607.E+6         *coef7,
          0.08606        ,612.E+6         *coef7,
          0.091085       ,617.E+6         *coef7,
          ))

#
# COURBE DE TRACTION � LA TEMP�RATURE 200�C
#
coef8=coef_unit(-6)
_A8=DEFI_FONCTION(
             NOM_PARA='EPSI',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
                 VALE=(
          2.16062E-3     ,417.E+6         *coef8,
          7.27979E-3     ,440.E+6         *coef8,
          0.012373       ,458.E+6         *coef8,
          0.0174456      ,472.E+6         *coef8,
          0.022523       ,487.E+6         *coef8,
          0.027591       ,500.E+6         *coef8,
          0.032653       ,512.E+6         *coef8,
          0.0377254      ,526.E+6         *coef8,
          0.0427876      ,538.E+6         *coef8,
          0.047829       ,546.E+6         *coef8,
          0.052876       ,555.E+6         *coef8,
          0.057912       ,562.E+6         *coef8,
          0.062943       ,568.E+6         *coef8,
          0.067979       ,575.E+6         *coef8,
          0.073005       ,580.E+6         *coef8,
          0.078031       ,585.E+6         *coef8,
          0.083057       ,590.E+6         *coef8,
          0.088078       ,594.E+6         *coef8,
          0.093104       ,599.E+6         *coef8,
          0.098124       ,603.E+6         *coef8,
          0.103135       ,605.E+6         *coef8,
          ))

#
# COURBE DE TRACTION � LA TEMP�RATURE 300�C
#
coef9=coef_unit(-6)
_A9=DEFI_FONCTION(
             NOM_PARA='EPSI',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
                 VALE=(
          2.13513E-3     ,395.E+6         *coef9,
          7.18919E-3     ,405.E+6         *coef9,
          0.0122973      ,425.E+6         *coef9,
          0.017405       ,445.E+6         *coef9,
          0.02254        ,470.E+6         *coef9,
          0.027649       ,490.E+6         *coef9,
          0.032735       ,506.E+6         *coef9,
          0.037811       ,520.E+6         *coef9,
          0.042881       ,533.E+6         *coef9,
          0.047935       ,543.E+6         *coef9,
          0.053          ,555.E+6         *coef9,
          0.058049       ,564.E+6         *coef9,
          0.06308        ,570.E+6         *coef9,
          0.068124       ,578.E+6         *coef9,
          0.073162       ,585.E+6         *coef9,
          0.078189       ,590.E+6         *coef9,
          0.08323        ,597.E+6         *coef9,
          0.08826        ,603.E+6         *coef9,
          0.093292       ,609.E+6         *coef9,
          0.098324       ,615.E+6         *coef9,
          0.10335        ,620.E+6         *coef9,
          ))

#
# COURBE DE TRACTION � LA TEMP�RATURE 400�C
#
coefA=coef_unit(-6)
_AA=DEFI_FONCTION(
             NOM_PARA='EPSI',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
                 VALE=(
          2.07386E-3     ,365.E+6         *coefA,
          7.24432E-3     ,395.E+6         *coefA,
          0.0123977      ,422.E+6         *coefA,
          0.017489       ,438.E+6         *coefA,
          0.022545       ,448.E+6         *coefA,
          0.027597       ,457.E+6         *coefA,
          0.0326364      ,464.E+6         *coefA,
          0.0376704      ,470.E+6         *coefA,
          0.04271023     ,477.E+6         *coefA,
          0.0477386      ,482.E+6         *coefA,
          0.052773       ,488.E+6         *coefA,
          0.057795       ,492.E+6         *coefA,
          0.062829       ,498.E+6         *coefA,
          0.0678523      ,502.E+6         *coefA,
          0.072875       ,506.E+6         *coefA,
          0.0778977      ,510.E+6         *coefA,
          0.0829148      ,513.E+6         *coefA,
          0.087937       ,517.E+6         *coefA,
          0.092954       ,520.E+6         *coefA,
          0.097977       ,524.E+6         *coefA,
          0.1029943      ,527.E+6         *coefA,
          ))

#
# COURBE DE TRACTION � LA TEMP�RATURE 500�C
#
coefB=coef_unit(-6)
_AB=DEFI_FONCTION(
             NOM_PARA='EPSI',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
                 VALE=(
          1.95783E-3     ,325.E+6         *coefB,
          7.15663E-3     ,358.E+6         *coefB,
          0.0122530      ,374.E+6         *coefB,
          0.017319       ,385.E+6         *coefB,
          0.022355       ,391.E+6         *coefB,
          0.027385       ,396.E+6         *coefB,
          0.032410       ,400.E+6         *coefB,
          0.0374337      ,404.E+6         *coefB,
          0.0424518      ,407.E+6         *coefB,
          0.0474638      ,409.E+6         *coefB,
          0.052476       ,411.E+6         *coefB,
          0.057482       ,412.E+6         *coefB,
          0.062488       ,413.E+6         *coefB,
          0.087512       ,417.E+6         *coefB,
          0.1025241      ,419.E+6         *coefB,
          ))


#
# COURBE DE TRACTION � LA TEMP�RATURE 600�C
#
coefC=coef_unit(-6)
_AC=DEFI_FONCTION(
             NOM_PARA='EPSI',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
                 VALE=(
          1.35484E-3     ,210.E+6         *coefC,
          6.46452E-3     ,227.E+6         *coefC,
          0.0115097      ,234.E+6         *coefC,
          0.016529       ,237.E+6         *coefC,
          0.021548       ,240.E+6         *coefC,
          0.031574       ,244.E+6         *coefC,
          0.041587       ,246.E+6         *coefC,
          0.10161935     ,251.E+6         *coefC,
          ))

#
# COURBE DE TRACTION � LA TEMP�RATURE 650�C
#
coefD=coef_unit(-6)
_AD=DEFI_FONCTION(
             NOM_PARA='EPSI',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
                 VALE=(
          1.42857E-3     ,150.E+6         *coefD,
          3.930952E-3    ,150.25E+6       *coefD,
          6.430952E-3    ,150.25E+6       *coefD,
          8.93210E-3     ,150.37E+6       *coefD,
          0.011433       ,150.5E+6        *coefD,
          0.021438       ,151.E+6         *coefD,
          0.03144286     ,151.5E+6        *coefD,
          0.04144762     ,152.E+6         *coefD,
          0.0514524      ,152.5E+6        *coefD,
          0.0764643      ,153.75E+6       *coefD,
          0.1014762      ,155.E+6         *coefD,
          ))

#
# COURBE DE TRACTION � LA TEMP�RATURE 700�C
#
coefE=coef_unit(-6)
_AE=DEFI_FONCTION(
             NOM_PARA='EPSI',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
                 VALE=(
          1.34146E-3     ,110.E+6         *coefE,
          0.01134756     ,110.5E+6        *coefE,
          0.0213537      ,111.E+6         *coefE,
          0.031360       ,111.5E+6        *coefE,
          0.041366       ,112.E+6         *coefE,
          0.051372       ,112.5E+6        *coefE,
          0.0613793      ,113.1E+6        *coefE,
          0.0713854      ,113.6E+6        *coefE,
          0.0813915      ,114.1E+6        *coefE,
          0.091398       ,114.6E+6        *coefE,
          ))

#
# COURBE DE TRACTION � LA TEMP�RATURE 750�C
#
coefF=coef_unit(-6)
_AF=DEFI_FONCTION(
             NOM_PARA='EPSI',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
                 VALE=(
          1.375E-3       ,88.E+6          *coefF,
          0.031375       ,88.E+6          *coefF,
          ))

#
# COURBE DE TRACTION � LA TEMP�RATURE 800�C
#
coefG=coef_unit(-6)
_AG=DEFI_FONCTION(
             NOM_PARA='EPSI',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
                 VALE=(
          1.25E-3        ,65.E+6          *coefG,
          0.03125        ,65.E+6          *coefG,
          ))

#
# COURBE DE TRACTION � LA TEMP�RATURE 1250�C
#
coefH=coef_unit(-6)
_AH=DEFI_FONCTION(
             NOM_PARA='EPSI',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
                 VALE=(
          4.E-3          ,2.E+6           *coefH,
          0.104          ,2.E+6           *coefH,
          ))

#
# COURBE DE TRACTION
#

_AI=DEFI_NAPPE(
             NOM_PARA='TEMP',
         PROL_DROITE='EXCLU',
         PROL_GAUCHE='EXCLU',
           PARA=(-170.,20.,100.,200.,300.,400.,500.,600.,
                  650.,700.,750.,800.,1250., ),
           FONCTION=(_A5,_A6,_A7,_A8,_A9,_AA,_AB,_AC,_AD,_AE,
                     _AF,_AG,_AH, )
         )

# COURBE DE FATIGUE DE WOHLER ISSUE DE RCCM Z1.4.1

_AZ=DEFI_FONCTION(NOM_PARA='SIGM',
                     INTERPOL='LOG',
                     VALE=(
                            86.0E6, 1000000.0,
                            93.0E6, 500000.0,
                           114.0E6, 200000.0,
                           138.0E6, 100000.0,
                           160.0E6, 50000.0,
                           215.0E6, 20000.0,
                           260.0E6, 10000.0,
                           330.0E6, 5000.0,
                           440.0E6, 2000.0,
                           570.0E6, 1000.0,
                           725.0E6, 500.0,
                          1070.0E6, 200.0,
                          1410.0E6, 100.0,
                          1900.0E6, 50.0,
                          2830.0E6, 20.0,
                          4000.0E6, 10.0 ,
                          ),);
_BZ=CALC_FONCTION(INVERSE=_F(FONCTION=_AZ,),);
_CZ=CALC_FONCTION(COMB=_F(FONCTION = _BZ,
                         COEF     =  coef_unit(-6)
                         ),);
DETRUIRE( CONCEPT =_F( NOM=_AZ), )
_AZ=CALC_FONCTION(INVERSE=_F(FONCTION=_CZ,),);



motscles = defi_motscles(
              THER=_F(
               extraction=True,
               RHO_CP  = temp_eval(_A1),
               LAMBDA  = temp_eval(_A0),
               ),
              THER_NL=_F(
               extraction=False,
               RHO_CP = _A1,
               LAMBDA = _A0,
               ),
              ELAS=_F(
               extraction=True,
               E  = temp_eval(_A2),
               NU  = temp_eval(_A3),
               ALPHA  = temp_eval(_A4),
               ),
              ELAS_FO=_F(
               extraction=False,
               E = _A2,
               NU = _A3,
               ALPHA = _A4,
               TEMP_DEF_ALPHA = 20.,
               ),
              TRACTION=_F( SIGM = _AI,    ),

              FATIGUE=_F(WOHLER=_AZ,
                         E_REFE=2.07E11*coef_unit(-6)
                        ),

          )

#


#  MODIF    DATE 05/07/2011   AUTEUR FERNANDES R.FERNANDES 
#            CONFIGURATION MANAGEMENT OF EDF VERSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2011  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY  
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY  
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR     
# (AT YOUR OPTION) ANY LATER VERSION.                                                  
#                                                                       
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT   
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF            
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              
#                                                                       
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,         
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        
# ======================================================================
# -*- coding: iso-8859-1 -*-
##################################################################
#                                                                #
#  Donnees assemblage de conception AFAXL pour les tubes-guide   #
#                                                                #
##################################################################
# Temperature de reference #
############################
_TP_REF   = 20. ;
_TP_MATF  = 20. ;
_TP_MATC  = 325.;
_NBTG     = 25 ;

######################################################
#   Tube guide                                       #
#   ----------                                       #
#   1 - Module d'Young a 20 C                        #
#   2 - Module d'Young a 325 C                       #
#   3 - Coefficient de Poisson                       #
#   4 - Coefficient de dilatation thermique          #
#   5 - Masse volumique                              #
######################################################
_ETU0  = 9.84e+10 ;
_ETU1  = 7.80e+10 ;
_NUTU0 = 0.3 ;
_ALTU0 = 5.37e-06 ;
_RHOTU = 6310. ;

# Fluage des tubes-guides (loi REFLET)
_A_TU  = 8.51E-9;
_B_TU  = 8.27E-9;
_W_TU  = 25.118;
_Q_TU  = 5000.;

# Loi de grandissement tube-guide
_ATU  =  0.0 ;
_BTU  =  0.0 ;
_STU  =  0.0 ;

######################################################
#   DEFINITION DES MATERIAUX AVEC FLUX NEUTRONIQUE   #
######################################################
#   TUBE GUIDE                                       #
######################################################

_E_TU =  DEFI_FONCTION(NOM_PARA    = 'TEMP',
                      VALE        =(_TP_MATF,_ETU0*_NBTG,_TP_MATC,_ETU1*_NBTG),
                      PROL_DROITE = 'CONSTANT',
                      PROL_GAUCHE = 'CONSTANT',);

_NU_TU = DEFI_FONCTION(NOM_PARA    = 'TEMP',
                      VALE        =(_TP_MATF, _NUTU0, _TP_MATC, _NUTU0),
                      PROL_DROITE = 'CONSTANT',
                      PROL_GAUCHE = 'CONSTANT',);

_AL_TU = DEFI_FONCTION(NOM_PARA    = 'TEMP',
                      VALE        =(_TP_MATF, _ALTU0, _TP_MATC, _ALTU0),
                      PROL_DROITE = 'CONSTANT',
                      PROL_GAUCHE = 'CONSTANT',);

motscles = defi_motscles(
                        ELAS_FO=_F(
                                    extraction = False,
                                    E              =  _E_TU,
                                    NU             =  _NU_TU,
                                    RHO            =  _RHOTU*_NBTG,
                                    ALPHA          =  _AL_TU,
                                    TEMP_DEF_ALPHA =  _TP_REF),
                       GRAN_IRRA_LOG=_F(
                                        extraction = False,
                                        A        =  _A_TU/_NBTG ,
                                        B        =  _B_TU/_NBTG ,
                                        CSTE_TPS =  _W_TU,
                                        FLUX_PHI =  1.,
                                        ENER_ACT =  _Q_TU,
                                        GRAN_A=_ATU,
                                        GRAN_B=_BTU,
                                        GRAN_S=_STU,),);

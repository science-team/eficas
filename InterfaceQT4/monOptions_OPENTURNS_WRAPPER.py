# -*- coding: utf-8 -*-

import os, re, sys

from PyQt4.QtGui  import *
from PyQt4.QtCore import *

from OptionsOT import Ui_desOptions


class desOptions(Ui_desOptions,QDialog):
   def __init__(self,parent ,modal ) :
       QDialog.__init__(self,parent)
       self.setupUi(self)
       self.setModal(modal)

class Options(desOptions):
   def __init__(self,parent = None,modal = 0,configuration=None):
       desOptions.__init__(self,parent,modal)
       self.configuration=configuration
       self.viewMan=parent
       self.dVersion={}
       self.dRepCat={}
       self.connecterSignaux()
       self.code='OPENTURNS_WRAPPER'
       self.initAll()
  
   def connecterSignaux(self) :
       self.connect(self.CBVersions,SIGNAL("activated(int)"),self.VersionChoisie)
       self.connect(self.Bdefaut,SIGNAL("clicked()"),self.BdefautChecked)
       self.connect(self.LEVersionAjout,SIGNAL("returnPressed()"),self.AjoutVersion)
       self.connect(self.PBajout,SIGNAL("clicked()"),self.AjoutVersion)
       self.connect(self.LEVersionSup,SIGNAL("returnPressed()"),self.SupVersion)
       self.connect(self.PBSup,SIGNAL("clicked()"),self.SupVersion)
       self.connect(self.LERepDoc,SIGNAL("returnPressed()"),self.ChangePathDoc)
       self.connect(self.LERepOT,SIGNAL("returnPressed()"),self.ChangePathOT)
       self.connect(self.LERepCata,SIGNAL("returnPressed()"),self.BokClicked)
       self.connect(self.LESaveDir,SIGNAL("returnPressed()"),self.ChangeSaveDir)
       self.connect(self.Bok,SIGNAL("clicked()"),self.BokClicked)
       self.connect(self.PBQuit,SIGNAL("clicked()"),self.close)


   def initAll(self):
       self.CBVersions.clear()
       for item in self.configuration.catalogues :
           try :
              (code,version,cata,format,defaut)=item
           except :
              (code,version,cata,format)=item
           self.dVersion[version]=(item)
           self.dRepCat[version]=str(cata)
           self.CBVersions.addItem(QString(version))
       self.LERepCata.setText(self.dRepCat[version])

       if hasattr(self.configuration,"path_doc"):
          self.LERepDoc.setText(self.configuration.path_doc)
       if hasattr(self.configuration,"OpenTURNS_path"):
          self.LERepOT.setText(self.configuration.OpenTURNS_path)
       if hasattr(self.configuration,"savedir"):
          self.LESaveDir.setText(self.configuration.savedir)


        
   def VersionChoisie(self):
       version=str(self.CBVersions.currentText())
       if self.dRepCat.has_key(version):
          self.LERepCata.setText(self.dRepCat[version])

   def BokClicked(self):
       version=str(self.CBVersions.currentText())
       if self.LERepCata.text() == "" :
          QMessageBox.critical( self, "Champ non rempli","Le champ Catalogue  doit etre rempli" )
          return
       if not os.path.isfile(self.LERepCata.text()) :
          res = QMessageBox.warning( None,
                 self.trUtf8("Fichier Catalogue "),
                 self.trUtf8("Le Fichier  n existe pas. Voulez-vous supprimer cette version ?"),
                 self.trUtf8("&Oui"),
                 self.trUtf8("&Non"))
          if res == 0 :
             self.LEVersionSup.setText(version)
             self.SupVersion()
             return

       self.dRepCat[version]=str(self.LERepCata.text())
       if version in self.dVersion.keys():
          item=list(self.dVersion[version])
          item[2]=self.dRepCat[version]
          self.dVersion[version]=tuple(item)
       else :
          self.dVersion[version]=(self.code,version,self.dRepCat[version],self.code.lower())
          
       lItem=[]
       for version in self.dVersion.keys() :
          lItem.append(self.dVersion[version])
       self.configuration.catalogues=lItem
       self.configuration.save_params()

   def AjoutVersion(self):
       version=self.LEVersionAjout.text()
       if str(version) == "" : return
       self.CBVersions.addItem(version)
       self.LERepCata.setText("")
       self.LEVersionAjout.setText("")
       self.CBVersions.setCurrentIndex(self.CBVersions.count()-1)

   def SupVersion(self):
       version=str(self.LEVersionSup.text())
       if version == "" : return
       i =0
       while i < self.CBVersions.count() :
           if  self.CBVersions.itemText(i) == version :
               self.CBVersions.removeItem(i)
               break
           i=i+1
       try :
          del self.dVersion[version]
          del self.dRepCat[version]
       except :
          self.LEVersionSup.setText("")
          try :
             self.CBVersions.setCurrentIndex(self.CBVersions.count()-1)
             self.VersionChoisie()
          except :
             pass
          return
       codeSansPoint=re.sub("\.","",version)
       chaine="rep_mat_"+codeSansPoint
       if hasattr(self.configuration,chaine):
          delattr(self.configuration,chaine)
       self.LEVersionSup.setText("")

       lItem=[]
       for version in self.dVersion.keys() :
           lItem.append(self.dVersion[version])
       self.LERepCata.setText("")
       self.configuration.catalogues=lItem
       self.configuration.save_params()
       self.CBVersions.setCurrentIndex(0)
       self.VersionChoisie()


   def BdefautChecked(self):
       res = QMessageBox.warning( None,
                 self.trUtf8("Restauration des parametres par defaut "),
                 self.trUtf8("Votre fichier editeur sera ecrase."),
                 self.trUtf8("&Ok"),
                 self.trUtf8("&Abandonner"))
       self.Bdefaut.setCheckState(Qt.Unchecked)
       if res == 1 : return 

       appli=self.configuration.appli
       fic_ini_util=self.configuration.fic_ini_utilisateur
       old_fic_ini_util=fic_ini_util+"_old"
       commande="mv "+fic_ini_util+" "+old_fic_ini_util
       os.system(commande)
       name='prefs_'+self.code
       prefsCode=__import__(name)
       nameConf='configuration_'+self.code
       configuration=__import__(nameConf)

       configNew=configuration.CONFIG(appli,prefsCode.repIni)
       self.configuration=configNew
       appli.CONFIGURATION=configNew
       self.configuration.save_params()
       self.dVersion={}
       self.dRepCat={}
       self.initAll()

   def ChangePathDoc(self):
       if self.LERepDoc.text()=="" : return
       if not os.path.isdir(self.LERepDoc.text()) :
          res = QMessageBox.warning( None,
                 self.trUtf8("Repertoire de Documentation "),
                 self.trUtf8("Le Repertoire  n existe pas."),
                 self.trUtf8("&Ok"),
                 self.trUtf8("&Abandonner"))
          if res == 1 :
             if hasattr(self.configuration,"path_doc"):
                self.LERepDoc.setText(self.configuration.path_doc)
             return

       self.configuration.path_doc=str(self.LERepDoc.text())
       self.configuration.save_params()

   def ChangePathOT(self):
       if not os.path.isdir(self.LERepOT.text()) :
          res = QMessageBox.warning( None,
                 self.trUtf8("Repertoire Open TURNS "),
                 self.trUtf8("Le Repertoire  n existe pas."),
                 self.trUtf8("&Ok"),
                 self.trUtf8("&Abandonner"))
          if res == 1 :
             if hasattr(self.configuration,"OpenTURNS_path"):
                self.LERepOT.setText(self.configuration.OpenTURNS_path)
             return

       if hasattr(self.configuration,"OpenTURNS_path"):
          sys.path.remove(self.configuration.OpenTURNS_path)
       self.configuration.OpenTURNS_path=str(self.LERepOT.text())
       self.configuration.save_params()
       if self.configuration.OpenTURNS_path == "" : return
       sys.path[:0]=[self.configuration.OpenTURNS_path]

   def ChangeSaveDir(self):
       if not os.path.isdir(self.LESaveDir.text()) :
          res = QMessageBox.warning( None,
                 self.trUtf8("Repertoire Open TURNS "),
                 self.trUtf8("Le Repertoire  n existe pas."),
                 self.trUtf8("&Ok"),
                 self.trUtf8("&Abandonner"))
          if res == 1 :
             if hasattr(self.configuration,"savedir"):
                self.LESaveDir.setText(self.configuration.savedir)
       self.configuration.savedir=str(self.LESaveDir.text())
       self.configuration.save_params()

